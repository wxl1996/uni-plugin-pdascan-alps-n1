package com.yunshu.uhf.uniplugin_scan;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;

import com.hc.pda.HcPowerCtrl;
import com.nlscan.nlsdk.NLDevice;
import com.nlscan.nlsdk.NLDeviceStream;

import io.dcloud.feature.uniapp.AbsSDKInstance;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

public class UHFScanModule extends UniModule {
    private UHFScanHelper uhfScanHelper = new UHFScanHelper();

    /**
     * 初始化
     */
    @UniJSMethod(uiThread = false)
    public void initScanBarcodeN1(UniJSCallback callback) {
        uhfScanHelper.initScanBarcodeN1(mUniSDKInstance, callback);
    }

    /**
     * 开始扫码
     */
    @UniJSMethod(uiThread = false)
    public void startScanBarcodeN1(final UniJSCallback onChangeCallback) {
        uhfScanHelper.startScanBarcodeN1(onChangeCallback);
    }

    /**
     * 停止扫码
     */
    @UniJSMethod(uiThread = false)
    public void stopScanBarcodeN1() {
        uhfScanHelper.stopScanBarcodeN1();
    }

    /**
     * 结束扫码
     */
    @UniJSMethod(uiThread = false)
    public void closeScanBarcodeN1() {
        uhfScanHelper.closeScanBarcodeN1();
    }

}


class UHFScanHelper implements NLDeviceStream.NLUartListener {
    public AbsSDKInstance mUniSDKInstance;
    private NLDeviceStream ds = new NLDevice(NLDeviceStream.DevClass.DEV_UART);
    private String path = "/dev/ttyS1";
    private String TAG = "TAG";
    private int baudrate = 9600;
    private UniJSCallback changeCallback = null;
    HcPowerCtrl ctrl = new HcPowerCtrl();
    public static SoundPool mSoundPool;
    public static Vibrator vibrator;
    public static int soundId = 1;

    public void initScanBarcodeN1(AbsSDKInstance _mUniSDKInstance, UniJSCallback callback) {
        mUniSDKInstance = _mUniSDKInstance;
        initSound();
        JSONObject res = new JSONObject();
        if (ctrl == null) {
            ctrl = new HcPowerCtrl();
        }
        ctrl.scanTrig(1);
        ctrl.scanPower(1);
        ctrl.scanWakeup(1);
        ctrl.scanPwrdwn(1);
        ctrl.scanTrig(0);
        SystemClock.sleep(100);
        boolean open = ds.open(path, baudrate, this);
        boolean b = ds.setConfig("@SCNTCE1");//设置指令模式
        boolean b1 = ds.setConfig("@232BAD8");//设置115200
        Log.e(TAG, "设置指令模式: " + b);
        Log.e(TAG, "设置115200: " + b1);
        if (open) {
            ds.close();
            SystemClock.sleep(50);
            boolean open2 = ds.open(path, 115200, this);
            res.put("status", 200);
            res.put("msg", "初始化成功");
            callback.invoke(res);
        } else {
            res.put("status", 201);
            res.put("msg", "初始化失败");
            callback.invoke(res);
        }
    }

    public void startScanBarcodeN1(final UniJSCallback onChangeCallback) {
        changeCallback = onChangeCallback;
        ctrl.scanTrig(1);
        ctrl.scanPower(1);
        ctrl.scanWakeup(1);
        ctrl.scanPwrdwn(1);
        ctrl.scanTrig(0);
        SystemClock.sleep(100);
        ds.startScan();
        Log.e(TAG, "开始========");
    }

    public void stopScanBarcodeN1() {
        changeCallback = null;
        ctrl.scanPower(0);
        ds.stopScan();
        Log.e(TAG, "停止========");
    }

    public void closeScanBarcodeN1() {
        changeCallback = null;
        ctrl.scanPower(0);
        ds.stopScan();
        ds.close();
        Log.e(TAG, "关闭========");
    }

    private byte[] barcodeBuff = new byte[2 * 1024];

    @Override
    public void actionRecv(byte[] RecvBuff, int len) {
        if (RecvBuff != null) {
            System.arraycopy(RecvBuff, 0, barcodeBuff, 0, len);
            String barcode = new String(barcodeBuff, 0, len);
            Log.e(TAG, "actionRecv: " + barcode);
            mSoundPool.play(soundId, 1, 1, 0, 0, (float) 1.0);
            JSONObject data = new JSONObject();
            data.put("code", barcode);
            if (changeCallback != null) changeCallback.invokeAndKeepAlive(data);
        } else {
            Log.e(TAG, "actionRecv: 返回数据null");
        }
    }

    private void initSound() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA).build();
            mSoundPool = new SoundPool.Builder().setMaxStreams(1).setAudioAttributes(audioAttributes).build();
        } else {
            mSoundPool = new SoundPool(1, AudioManager.STREAM_SYSTEM, 10);
        }
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Log.e(TAG, "初始化音效成功");
            vibrator = (Vibrator) mUniSDKInstance.getContext().getSystemService(mUniSDKInstance.getContext().VIBRATOR_SERVICE);
            soundId = mSoundPool.load(mUniSDKInstance.getContext().getApplicationContext(), R.raw.dingdj5, 10);
        }
    }

}
