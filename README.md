此项目为 uni 原生插件，为 alps 品牌的 PDA 提供扫码功能，对应扫码模块型号是 N1。

---

## 注意事项

- **测试步骤**

  打开 Android studio，运行 app，按下设备右侧橘黄色按钮即可测试单次扫码
  <br/>

- **开发资料**

  创建 Module 时填写的 `Module Name` 为 `uniplugin_scan`，`Package Name` 为 `com.yunshu.uhf.uniplugin_scan`，创建完成后需要修改 `build.gradle`、`proguard-rules.pro`、`AndroidManifest.xml`。
  项目中用到了 2 个依赖`nlsdk-debug.aar`、`serial-port-debug.aar`在 libs 下，需要同步更新`build.gradle`中的`dependencies`。

  生成插件的 aar：选择 Gradle--->插件 module--->Tasks--->build 或 other--->assembleRelease 编译 module 的 aar 文件，生成的文件在 module\build\outputs\aar，放到 uniapp 工程的 nativePlugins/module/android 文件夹下（注意`minSdkVersion`不能小于插件设置的`minSdkVersion`）

  `app\src\main\assets\dcloud_uniplugins.json` 配置 uniapp 要用的原生插件
  `app\build.gradle` implementation project 引入 Android Library 类型的 Module 作为依赖库
  `settings.gradle` 用于将项目添加至编译项目

  Android 插件开发教程：https://nativesupport.dcloud.net.cn/NativePlugin/course/android.html
  ABI 类型：https://blog.csdn.net/weixin_45112340/article/details/129236358
